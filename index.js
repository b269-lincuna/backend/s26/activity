
{
    "firstName":"John",
    "lastName":"Doe",
    "email":"johndoe@gmail.com",
    "password":"secret",
    "isAdmin":"false",
    "mobileNumber":"09876543210"
}

{
    "firstName":"Jane",
    "lastName":"Doe",
    "email":"janedoe@gmail.com",
    "password":"secret2",
    "isAdmin":"true",
    "mobileNumber":"09876543211"
}

{
    "userId":"1",
    "transactionDate":"3/27/2023",
    "status":"successful",
    "total":"175.00"
}

{
    "userId":"1",
    "transactionDate":"3/27/2023",
    "status":"pending",
    "total":"190.00"
}

{
    "Name":"Milk",
    "Desctiption":"Fresh milk",
    "Price":"10.00",
    "Stocks":"70",
    "IsActive":"true",
    "SKU":"SKU-001-001"
}

{
    "Name":"Coffee",
    "Desctiption":"3 in 1",
    "Price":"15.00",
    "Stocks":"150",
    "IsActive":"true",
    "SKU":"SKU-001-002"
}

{
    "Name":"Sugar",
    "Desctiption":"Washed",
    "Price":"20.00",
    "Stocks":"200",
    "IsActive":"true",
    "SKU":"SKU-001-003"
}

{
    "OrderID":"1", 
    "ProductID":"1",
    "Quantity":"10.00",
    "Price":"10.00",
    "SubTotal":"100.00"
}

{
    "OrderID":"1",
    "ProductID":"2",
    "Quantity":"5.00",
    "Price":"15.00",
    "SubTotal":"75.00"
}

{
    "OrderID":"2",
    "ProductID":"1",
    "Quantity":"5.00",
    "Price":"10.00",
    "SubTotal":"50.00"
}

{
    "OrderID":"2",
    "ProductID":"3",
    "Quantity":"7.00",
    "Price":"20.00",
    "SubTotal":"140.00"
}

